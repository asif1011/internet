# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.db import models
from django.forms import TextInput, Textarea


# Register your models here.

from .models import Category


class CategoryAdmin(admin.ModelAdmin):
    list_display = ["name", "parent", "is_featured", "is_active"]
    search_fields = ['name']

    description = {
        models.TextField: {'widget': Textarea(attrs={'rows':4, 'cols':40})},
    }

admin.site.register(Category, CategoryAdmin)