# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.db import models
from django.contrib.auth.models import User

from storages.backends.s3boto3 import S3Boto3Storage
from core.utils import PublicMediaStorage


class CommonInfo(models.Model):
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        abstract = True


class Category(CommonInfo):
    name = models.CharField(max_length=60)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='parent_category')
    is_featured = models.BooleanField(default=False)
    image = models.ImageField(upload_to="image/", storage=PublicMediaStorage())
    is_active = models.BooleanField(default=False)
    description = models.TextField(max_length=500)
    
    def __str__(self):
        return self.name