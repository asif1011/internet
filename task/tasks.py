# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import time

from core.celery import app
from celery import Task
from celery.utils.log import get_task_logger
from django.core.mail import EmailMessage



logger = get_task_logger(__name__)



class SendEmail(Task):
    ignore_result = False
    name = 'task.tasks.send_email'

    def run(self, *args, **kwargs):

        name = kwargs["name"]
        category_id = kwargs["category_id"]
        time.sleep(900)

        try:
            hyperlink = 'http://13.232.232.131/v1/api/category/{}/'.format(category_id)
            email = EmailMessage(
                'New Category "{}" has been added.'.format(name),
                'Hi Naveen a new category has been added, click on the category {} to see'.format(hyperlink),
                to=['aakhtar189@gmail.com']
            )
            email.send()

        except Exception as error:
            print ('Caught this error: ' + repr(error))
            raise Exception(repr(error))


app.tasks.register(SendEmail())


