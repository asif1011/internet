from django.conf.urls import url
from api.v1_0.task import views 

urlpatterns = [

    url(r'^categories/$', views.CategoryView.as_view(), name='categories'),
    url(r'^category/(?P<pk>[0-9]+)/$', views.DetailCategoryView.as_view(), name='detail_category'),
]

