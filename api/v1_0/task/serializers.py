from datetime import datetime


from rest_framework import serializers, exceptions

from django.conf import settings
from django.contrib.auth.models import User
from task.models import Category


class CategorySerializer(serializers.ModelSerializer):
    parent = serializers.PrimaryKeyRelatedField(allow_null=True, queryset=Category.objects.filter(parent__isnull=True)) 
    user = serializers.SerializerMethodField(read_only=True)
    authenticated = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Category
        fields = "__all__"

    def validate(self, data):
        if Category.objects.filter(name=data['name']).exists():
            raise serializers.ValidationError({"name": "{} Category already exists".format(data['name'])})
        return data

    def get_user(self, obj):
        return self.context.get("user", "")

    def get_authenticated(self, obj):
        return self.context.get("authenticated", "")