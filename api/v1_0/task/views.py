from django.contrib.auth.models import User
 
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework.decorators import api_view, permission_classes

from task.models import Category
from api.v1_0.task.serializers import CategorySerializer

from task.tasks import SendEmail


class CategoryView(generics.ListCreateAPIView):
    permission_classes = ()
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('id', 'name', 'is_featured')
    ordering_fields = '__all__'


    def get_serializer_context(self):
        if self.request.user.username == "":
            user = None
            authenticated = False
        else:
            user = self.request.user.username
            authenticated = True
        return {"user": user, "authenticated": authenticated}


    def perform_create(self, serializer):
        instance = serializer.save()
        return SendEmail().delay(category_id=instance.id, name=instance.name)


class DetailCategoryView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = ()
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def perform_update(self, serializer):
        old_image = serializer.instance.image
        instance = serializer.save()
        if not instance.image and old_image:
            instance.image = old_image
            instance.save()